function WhoAmI {
    # Get the current Windows user
    $currentUser = [Security.Principal.WindowsPrincipal]::new([Security.Principal.WindowsIdentity]::GetCurrent())
 
    # Get the SID (Security Identifier) of the Administrators group
    $adminsSid = (Get-WmiObject -Class Win32_Group -Filter "name='Administrators'").SID
 
    # Get the username of the current user
    $Uname = ($currentUser.Identities.name)
 
    # Check if the current user is a member of the Administrators group
    if ($currentUser.IsInRole($adminsSid)) {
       # If true, print a message indicating that the current user is an administrator and return $true
       Write-Host "Current user, $Uname, is an administrator"
       return $true
    } else {
       # If false, print a message indicating that the current user is not an administrator and return $false
       Write-Host "Current user, $Uname, is not an administrator"
       return $false
    }
 }
 